#!/bin/sh

orekit_version=v10_3_1_0
python_orekit_version='10.3.1'

wget https://github.com/petrushy/orekit_python_artifacts/archive/$orekit_version.zip
unzip $orekit_version.zip

DEFAULT_JAVA_PREFIX=/usr/lib/jvm/java-8-openjdk-amd64

if [ -z "$JAVA_PREFIX" ]; then
  JAVA_PREFIX=${DEFAULT_JAVA_PREFIX}
fi

export SRC_DIR=`ls | grep orekit_python_artifacts-`
export SRC_DIR=`pwd`/$SRC_DIR
export PREFIX=${JAVA_PREFIX}
export PYTHON=$(which python3)
OS="$(uname -s)"
OS_LOWER="$(echo $OS | awk '{print tolower($0)}')"

if [ "$OS_LOWER" = "darwin" ]; then
  OS_LOWER="macosx-12"
fi

if [ -z "$PYTHON_PKGS_DIR" ]; then
  if [ "$OS" = "Darwin" ]; then
    export PYTHON_PKGS_DIR="/usr/local/lib/python3.8/site-packages"
  else
    # NOTE: dist-packages location is only true for Ubuntu 20.04
    export PYTHON_PKGS_DIR="/usr/local/lib/python3.8/dist-packages"
  fi
fi

chmod -R +x $SRC_DIR
ls -alrt $SRC_DIR

$PYTHON -m jcc \
--use_full_names \
--python orekit \
--version ${python_orekit_version} \
--jar $SRC_DIR/orekit-10.3.1.jar \
--jar $SRC_DIR/hipparchus-clustering-1.8.jar \
--jar $SRC_DIR/hipparchus-core-1.8.jar \
--jar $SRC_DIR/hipparchus-fft-1.8.jar \
--jar $SRC_DIR/hipparchus-filtering-1.8.jar \
--jar $SRC_DIR/hipparchus-fitting-1.8.jar \
--jar $SRC_DIR/hipparchus-geometry-1.8.jar \
--jar $SRC_DIR/hipparchus-migration-1.8.jar \
--jar $SRC_DIR/hipparchus-ode-1.8.jar \
--jar $SRC_DIR/hipparchus-optim-1.8.jar \
--jar $SRC_DIR/hipparchus-stat-1.8.jar \
--jar $SRC_DIR/rugged-2.2.jar \
--package java.io \
--package java.util \
--package java.text \
--package org.orekit \
--package org.orekit.rugged \
java.io.BufferedReader \
java.io.FileInputStream \
java.io.FileOutputStream \
java.io.InputStream \
java.io.InputStreamReader \
java.io.ObjectInputStream \
java.io.ObjectOutputStream \
java.io.PrintStream \
java.io.StringReader \
java.io.StringWriter \
java.lang.System \
java.text.DecimalFormat \
java.text.DecimalFormatSymbols \
java.util.ArrayDeque  \
java.util.ArrayList \
java.util.Arrays \
java.util.Collection \
java.util.Collections \
java.util.Date \
java.util.HashMap \
java.util.HashSet \
java.util.List \
java.util.Locale \
java.util.Map \
java.util.Set \
java.util.TreeSet \
java.util.stream.Collectors \
java.util.stream.Stream \
java.util.stream.DoubleStream \
java.util.function.LongConsumer \
java.util.function.IntConsumer \
java.util.function.DoubleConsumer \
--module $SRC_DIR/pyhelpers.py \
--reserved INFINITE \
--reserved ERROR \
--reserved OVERFLOW \
--reserved NO_DATA \
--reserved NAN \
--reserved min \
--reserved max \
--reserved mean \
--reserved SNAN \
--classpath $PREFIX/lib/tools.jar \
--files 81 \
--build \
--install

ls -alrt ${PYTHON_PKGS_DIR}/orekit*

echo "Setting up directory structure for Python packaging"
mkdir ./orekit
cp -a "${PYTHON_PKGS_DIR}/orekit-10.3.1-py3.8-$OS_LOWER-x86_64.egg/orekit" .
if [ "$SHOULD_CLEANUP" = "true" ]; then
  rm -rf build $orekit_version.zip
fi

