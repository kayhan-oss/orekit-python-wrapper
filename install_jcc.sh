#!/bin/sh

DEFAULT_JAVA_PREFIX=/usr/lib/jvm/java-8-openjdk-amd64

if [ -z "$JAVA_PREFIX" ]; then
  JAVA_PREFIX=${DEFAULT_JAVA_PREFIX}
fi

export JCC_JDK=${JAVA_PREFIX}
export JCC_INCLUDES=${JAVA_PREFIX}/include

if [ -d ${JAVA_PREFIX}/include/linux ]; then
  export JCC_INCLUDES=${JCC_INCLUDES}:${JAVA_PREFIX}/include/linux
elif [ -d ${JAVA_PREFIX}/include/darwin ]; then
  export JCC_INCLUDES=${JCC_INCLUDES}:${JAVA_PREFIX}/include/darwin
fi

export PYTHON=$(which python3)

svn co https://svn.apache.org/repos/asf/lucene/pylucene/trunk/jcc jcc
cd jcc

JCC_JDK=${JCC_JDK}/jre $PYTHON setup.py build
JCC_JDK=${JCC_JDK}/jre $PYTHON setup.py install

cd ..
#rm -rf jcc
