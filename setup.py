from setuptools import setup
from setuptools.dist import Distribution
from setuptools.command.install import install

class InstallPlatlib(install):
    def finalize_options(self):
        install.finalize_options(self)
        if self.distribution.has_ext_modules():
            self.install_lib = self.install_platlib

class BinaryDistribution(Distribution):
    """Distribution which always forces a binary package with platform name"""
    def has_ext_modules(foo):
        return True

setup(
    name = 'orekit',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description = 'A Python wrapper for the Orekit Java library',
    url = 'https://gitlab.com/kayhan-oss/orekit-python-wrapper',
    author = 'Kayhan Space Corporation',
    author_email = 'dhathri@kayhan.space',
    license = 'Apache 2.0',
    include_package_data=True,
    python_requires='>=3.8',
    distclass=BinaryDistribution,
    packages = ['orekit'],
    cmdclass={'install': InstallPlatlib},
)

