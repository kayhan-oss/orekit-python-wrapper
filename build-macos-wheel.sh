#!/bin/bash -o pipefail

BREW=/usr/local/bin/brew
INSTALL="n"
PYTHON_BINDIR="/usr/local/opt/python@3.8/bin"
SHOULD_CLEANUP=true

function print-usage {
  cat << EOF >&2
usage: $0 [<options>]
    --brew-bin <path>                       Alternate path to an x86_64 `brew` binary.
                                            Value: $BREW
    --install-deps                          Don't prompt for installing package dependencies, just
                                            go ahead and do it.
                                            Value: $INSTALL
    --no-cleanup                            Don't clean up build artifacts after the build completes.
                                            Value: Cleanup? $SHOULD_CLEANUP
    --help                                  Print this help message.
EOF
}

while [[ $# -gt 0 ]]
do
  case $1 in
    --brew-bin)
      BREW="$2"
      shift
      ;;
    --install-deps)
      INSTALL="y"
      shift
      ;;
    --no-cleanup)
      SHOULD_CLEANUP=NO
      ;;
    --help)
      SHOW_HELP=YES
      ;;
    *)
      echo "error: unknown option '$1'" >&2
      exit 255
      ;;
  esac
  shift
done

if [ "$SHOW_HELP" = "YES" ]
then
  print-usage
  exit 255
fi

export BUILD_ROOT_DIR=$(pwd)

PKG_LIST="gcc git openjdk@8 libffi make openssl python@3.8 svn unzip wget zlib"
MISSING_PKG_LIST=""

echo "Checking dependencies..."
for pkg in ${PKG_LIST}; do
  bin="$(which $pkg)"
  brew_listed="$($BREW list | grep $pkg)"
  if [ -z "$bin" ] && [ -z "$brew_listed" ]; then
    MISSING_PKG_LIST="$MISSING_PKG_LIST $pkg"
  fi
done

if [ -n "$MISSING_PKG_LIST" ]; then
  cat << EOF >&2
  The following Homebrew packages will be installed on your computer:
  $MISSING_PKG_LIST
EOF

  if [ "$INSTALL" = "n" ]; then
    read -e -p "Ok? (y/n): " INSTALL
  fi

  if [ "$(echo $INSTALL | awk '{print tolower($0)}')" != "y" ]; then
    echo "Exiting..."
    exit 255
  fi
fi

if [ -n "$MISSING_PKG_LIST" ]; then
  echo "Installing packages..."
  set -x
  for pkg in $MISSING_PKG_LIST; do
    $BREW install $pkg
  done
  set +x
fi

echo "Python version:"
${PYTHON_BINDIR}/python3 --version

JAVA_DIR="/usr/local/opt/openjdk@8/libexec/openjdk.jdk/Contents/Home"
export JCC_JDK=${JAVA_DIR}/jre
export JCC_INCLUDES=${JAVA_DIR}/include:${JAVA_DIR}/include/darwin
#export CPPFLAGS="-I${JAVA_DIR}/include"

echo "Activating python virtual environment 'venv'"
${PYTHON_BINDIR}/python3 -m pip install virtualenv
${PYTHON_BINDIR}/python3 -m virtualenv venv
source ./venv/bin/activate

echo "Installing Python dependencies..."
${PYTHON_BINDIR}/python3 -m pip install setuptools setuptools_scm  wheel

echo "Building jcc..."
JAVA_PREFIX=${JAVA_DIR} ./install_jcc.sh

echo "Building orekit..."
PYTHON_PKGS_DIR="./venv/lib/python3.8/site-packages" JAVA_PREFIX=${JAVA_DIR} SHOULD_CLEANUP=$SHOULD_CLEANUP ./build_orekit_with_jcc.sh

ls -alrt
ls -alrt orekit

echo "Building python wheels..."
${PYTHON_BINDIR}/python3 setup.py bdist_wheel --plat-name=macosx_12_0_x86_64
${PYTHON_BINDIR}/python3 setup.py bdist_wheel --plat-name=macosx_11_0_x86_64
ls dist

echo "Done."
