# Orekit-Python-Wrapper

This is a project designed specifically to build and publish PyPI package for Orekit Java library.

Parts of the build scripts are borrowed from the official python-wrapper (https://gitlab.orekit.org/orekit-labs/python-wrapper) project from folks at Orekit. However, Orekit folks only publish a Conda package for the Orekit Java library. We have found that, having a PyPI package for this library is immensely useful. Hence, this open-source project was created at Kayhan Space Corporation.

To install this Orekit Python library use the following command:
`pip install orekit --extra-index-url https://gitlab.com/api/v4/projects/30693295/packages/pypi/simple`


